# SSH Key Pair

resource "tls_private_key" "private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "key_pair" {
  key_name   = "${var.ENVIRONMENT_NAME}_KEY_PAIR"
  public_key = tls_private_key.private_key.public_key_openssh

  tags = local.common_tags
}

# Instance Profile

data "aws_iam_policy_document" "s3-allow-crud-policy-doc" {
  statement {
    actions   = ["s3:GetObject", "s3:PutObject", "s3:DeleteObject"]
    resources = ["${aws_s3_bucket.s3_bucket.arn}/*"]
    effect    = "Allow"
  }
}

data "aws_iam_policy_document" "webapp-assume-role-policy-doc" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "webapp-policy" {
  name   = "webapp-policy-${var.SHORT_ENVIRONMENT_NAME}"
  policy = data.aws_iam_policy_document.s3-allow-crud-policy-doc.json
}

resource "aws_iam_role" "webapp-role" {
  name               = "webapp-role-${var.SHORT_ENVIRONMENT_NAME}"
  assume_role_policy = data.aws_iam_policy_document.webapp-assume-role-policy-doc.json
}

resource "aws_iam_role_policy_attachment" "rp_attachment" {
  role       = aws_iam_role.webapp-role.name
  policy_arn = aws_iam_policy.webapp-policy.arn
}


resource "aws_iam_instance_profile" "webapp_profile" {
  role = aws_iam_role.webapp-role.name
}




# EC2 Instance

data "aws_ami" "ubuntu_20_04" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  # Canonical
  owners = ["099720109477"]
}

resource "aws_instance" "webapp" {
  ami                    = data.aws_ami.ubuntu_20_04.id
  instance_type          = var.EC2_INSTANCE_TYPE
  key_name               = aws_key_pair.key_pair.key_name
  iam_instance_profile   = aws_iam_instance_profile.webapp_profile.name
  vpc_security_group_ids = [aws_security_group.security_group.id]
  subnet_id              = aws_subnet.subnet_primary.id

  tags = local.common_tags
}

# ElasticIP

resource "aws_eip" "public_ip" {
  instance = aws_instance.webapp.id

  tags = local.common_tags
}

# Output

output "public_ip" {
  value = aws_eip.public_ip.public_ip
}

output "private_key" {
  value     = tls_private_key.private_key
  sensitive = true
}
